
import { Component, OnInit } from '@angular/core';
import { BookService } from './services/book.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  



constructor(private books:BookService){}
result:any;

  ngOnInit(): void {

    this.books.getData().subscribe((result)=>{
       console.log(result);
       this.result=result;
    });
    
  }



}
