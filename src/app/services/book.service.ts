import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class BookService {

  url="https://fakestoreapi.com/products?limit=5"
  constructor(private https:HttpClient) { }

  getData(){
    return this.https.get(this.url);
  }

}
